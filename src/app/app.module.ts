import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import {UserComponent} from "./components/user/user.component";
import { MzButtonModule, MzInputModule } from 'ngx-materialize';
import {HttpClientModule} from "@angular/common/http";
import {AddNewComponent} from "./components/user/add-new/add-new.component";
import {EditComponent} from "./components/user/edit/edit.component";
import {FormControl} from "@angular/forms";
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { RegistateComponent } from './components/register/registate.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    UserComponent,
    AddNewComponent,
    EditComponent,
    RegistateComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    HttpClientModule,
    MzButtonModule,
    MzInputModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

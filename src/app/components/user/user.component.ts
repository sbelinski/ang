import {Component, OnInit} from '@angular/core';
import {UserService} from "./user.service";
import {UserInterface} from "./user.interface";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})

export class UserComponent implements OnInit {
  users: UserInterface[];
  id: string;
  constructor( private readonly userService: UserService,
               private route : ActivatedRoute) { }
  deleteOne(id){
      this.userService.deleteOneUser(id).subscribe(data => {this.users = data});
  }

  pushOne(newUser$){
    newUser$.subscribe(data =>{
      this.userService.postOneUser(data).subscribe(data => {this.users.push(data)});
    });
  }

 ngOnInit() {
      this.userService.getUsersAll().subscribe(data => this.users = data)
  }
}

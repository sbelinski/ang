import { Component, OnInit } from '@angular/core';
import {FormControl} from "@angular/forms";
import {UserService} from "../user.service";
import {Subject} from "rxjs";
import {UserComponent} from "../user.component";

@Component({
  selector: 'app-add-new',
  templateUrl: './add-new.component.html',
  styleUrls: ['./add-new.component.css']
})
export class AddNewComponent implements OnInit {

  constructor(  private readonly userService: UserService,
                private readonly userComponent: UserComponent) { }
  firstNameControl = new FormControl('');
  lastNameControl = new FormControl('');
  ageControl = new FormControl('');
  emailControl = new FormControl('');

  postOne() {
    const newUser = new Subject();
    const newUser$ = newUser.asObservable();
    const user = {
      firstName : this.firstNameControl.value,
      lastName : this.lastNameControl.value,
      email : this.emailControl.value,
      age : this.ageControl.value,
    };
    this.userComponent.pushOne(newUser$);
    newUser.next(user);
  }
  ngOnInit() {
  }

}

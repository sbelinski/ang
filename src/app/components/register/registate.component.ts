import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";

@Component({
  selector: 'app-registate',
  templateUrl: './registate.component.html',
  styleUrls: ['./registate.component.css']
})
export class RegistateComponent implements OnInit {

  constructor() { }

  registrationForm = new FormGroup({
    firstName: new FormControl(),
    lastName: new FormControl(),
    age: new FormControl(),
    email: new FormControl(),
    password: new FormControl(),
  });

  log(){
    console.log(this.registrationForm.value);

  }

  ngOnInit() {
  }

}
